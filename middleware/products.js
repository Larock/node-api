const express = require('express');
const router = express.Router();

const connection = require('../helpers/connection')
const query = require('../helpers/query')
const dbConfig = require('../dbConfig');

const getAllProducts = "select * FROM products";

router.get('/all', async(req, res) => {
  const conn = await connection(dbConfig).catch(e => {console.log(e)});
  const results = await query(conn, getAllProducts).catch(console.log);
  return res.json({results});
});

router.get('/feed', (req, res) => {
  res.send('Here get the feed');
});

router.get('/:id', (req, res) => {
  const { id } = req.params;
  res.send({ id });
});

module.exports = router;